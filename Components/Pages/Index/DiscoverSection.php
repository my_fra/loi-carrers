<section id="discover">
    <div class="text-discover raleway">
        <h3>Discover your fit!</h3>
        <p>
            <span>Does your personality match the values ​​of the Lion of Informatics?</span>
            <span>Take this simple quiz based on your interests and experiences to find the best career opportunities!</span>
        </p>
    </div>
    <div class="button-wrap">
        <button>Let’s get started</button>
    </div>
</section>