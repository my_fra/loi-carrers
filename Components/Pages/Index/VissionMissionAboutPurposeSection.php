<section class="container" id="VissionMissionAboutPurposeSection">
    <h2 class="raleway title-vission-mission-purpose">About Us</h2>
    <div class="vission-mission-about-purpose-wrapper">
        <?php require_once './Components/Pages/Index/VisionMissionSection.php' ?>
        <?php require_once './Components/Pages/Index/BenefitSection.php' ?>
    </div>
</section>