<section id="hero" class="poppins container-left">
    <div class="text-hero">
        <h1>Let's Join as Lion of Informatics</h1>
        <!-- <p>Delivering Impacts Take Real Actions !</p> -->
        <p>When We Build For The World, We Bring Our Talents, Experiences, And Passions With Us. Through Our Unique Stories, We Help Bring The World Closer Together.</p>
        <div>
            <a href="">Explore all jobs</a>
            <a href="">Discover your fit</a>
        </div>
    </div>
    <div class="bg-hero">
        <img loading="lazy" src="./assets/images/office/Meeting Room.webp" alt="Lion of Informatics">
    </div>
</section>