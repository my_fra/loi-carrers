<footer id="footer" class="container poppins">
    <span class="copyright">Copyright &copy; 2020 - <?php echo date('Y')?> Lion of Informatics <br> group of PT. Rizqullah Indonesia Sejahtera. All Rights Reserved</span>
    <ul class="nav-links">
        <li><a href="">Home</a></li>
        <li><a href="">About</a></li>
        <li><a href="">Privacy Policy</a></li>
        <li><a href="">Terms of Use</a></li>
    </ul>
</footer>