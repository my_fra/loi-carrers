<nav class="container">
    <a href="/" class="logo">
        <img loading="lazy" src="./assets/images/logo/lion-of-informatics.webp" alt="Lion of Informatics">
        <span class="poppins">Careers</span>
    </a>
    <ul class="nav-links poppins">
        <li>
            <a href="https://lionofinformatics.net/" target="_blank">About</a>
        </li>
        <li>
            <a href="https://careers.lionofinformatics.net/" target="_blank">Career</a>
        </li>
        <li>
            <a href="https://opensource.lionofinformatics.net/" target="_blank">Open Source</a>
        </li>
        <li>
            <a href="https://blog.lionofinformatic.com/" target="_blank">Blog</a>
        </li>
        <li>
            <a href="" class="button">Empty Vacancies</a>
        </li>
    </ul>

    <div class="hamburger" onclick="toggleSidebar(this)">
        <span></span>
        <span></span>
        <span></span>
    </div>
</nav>